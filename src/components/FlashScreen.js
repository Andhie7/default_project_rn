import { StyleSheet, Text, View, Image, StatusBar } from 'react-native';
import React from 'react';
import LinearGradient from "react-native-linear-gradient";
import { GlobalColors, GlobalFontSizes } from '../constants/Styles';
import { GlobalImages } from '../constants/Images';
import textStyles from '../constants/TextStyles';
import Icon from 'react-native-vector-icons/Ionicons';

export default function FlashScreen() {
    return (
        <LinearGradient
            style={styles.container}
            colors={[GlobalColors.DANGER, GlobalColors.RED]}
            start={{ x: 0.5, y: 0.1 }}
        >
            <StatusBar hidden={true} />
            <Icon name='thunderstorm' size={GlobalFontSizes[40]} color='white'/>
            <Text style={[textStyles.textBold20, { color: 'white' }]}>Default apps</Text>
        </LinearGradient>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    imageZ: {
        position: 'absolute',
        bottom: 10,
        height: "10%",
        width: "40%"
    },
})